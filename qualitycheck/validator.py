import json
import inspect


class InvalidDataException(Exception):
    pass


def is_check_method_predicate(object):

    """
        This is a predicate used to filter all methods whose names start
        with the string 'check'
    """

    return inspect.ismethod(object) and object.__name__.lower().startswith("check")


class DataValidator(object):

    """
        This class is used to verify that data inputed for various objects
        meets some quality standards.

         - Each check is done in its own function
         - Each function that starts with check_* will be run as a check,
           this is similar to how the unittest.Testcase class runs every
           test_* as a test

         - The class expects JSON as its input but the logic doesn't care,
           which allows use of other formats in the future.

         - Each check_* method has a severity attribute. For every failed check
           this attribute is added to a total. This total is monitored and it is only
           once a set minimum threshold is reached that an email will be sent out with
           the errors. The email is not sent by this class.
         - For every object that fails a check, we add to the object, the error messages,
           and add the object to a list, this list will be sent back to the user.

        This class can be used for validating other objects in the future
    """

    error_messages = {
        "invalid_data": "The data should either be a JSON array or"
                        " a JSON object",
        "not_implemented": "%s parser has not been implemented"
    }

    FAIL_SCORE_THRESHOLD = 0

    def __init__(self, recieved_product_data, data_format="json"):
        self.objects_to_validate = []

        self.objects_with_errors = []

        # for tracking statistics
        self.objects_tested = 0
        self.tests_ran = 0
        self.tests_passed = 0
        self.tests_failed = 0
        self.total_fail_score = 0

        if data_format.lower() == "json":
            self.parse_json_data(recieved_product_data)
        else:
            raise NotImplementedError(self.error_messages["not_implemented"] % data_format)

    def parse_json_data(self, data):
        try:
            parsed_data = json.loads(data)
        except ValueError:
            raise InvalidDataException(self.error_messages["invalid_data"])

        # The expected data could either be a JSON array or a JSON object
        # here we find out which type of object(s) we received and append
        # them/it to objects_to_validate
        if isinstance(parsed_data, dict):
            # check if we had a dict
            self.objects_to_validate.append(parsed_data)
        elif hasattr(parsed_data, "__iter__"):
            # check if data was array
            self.objects_to_validate = parsed_data
        else:
            # other valid json data types exist e.g number, boolean
            # we don't want those
            raise InvalidDataException(self.error_messages["invalid_data"])

    def run_checks_on_object(self, object):
        self.objects_tested += 1
        for method_name, method in \
                inspect.getmembers(self, predicate=is_check_method_predicate):

            try:
                self. tests_ran += 1
                method(object)
            except Exception as e:

                """
                    If an object fails a check we add to the tests_failed cound
                    We also append a key called errors
                """

                self.tests_failed += 1

                method_severity = getattr(method, "severity", 10)
                self.total_fail_score += method_severity

                object.setdefault("errors", []).append(e.message)

                self.objects_with_errors.append(object)
            else:
                self.tests_passed += 1

    def validate_data(self):

        for object in self.objects_to_validate:
            self.run_checks_on_object(object)

    @property
    def passed_threshold(self):
        return self.total_fail_score > self.FAIL_SCORE_THRESHOLD


class ProductDataValidator(DataValidator):

    FAIL_SCORE_THRESHOLD = 5

    def check_required_data_is_included(self, object):
        """
            For this example, the product must have a title and description
        """

        # empty strings are also unacceptable
        # this form of get makes sure that for example,
        # title will always be None if the data is unacceptable input
        # this makes the assert code much simpler
        title = object.get("title", "").strip() or None
        description = object.get("description", "").strip() or None

        assert None not in (title, description), "Please make sure the product" \
                                                 " has a title and description"

    check_required_data_is_included.severity = 10

    def check_title_no_whitespace_around(self, object):
        title = object.get("title", "")

        assert title == title.strip(), "Please make sure the title of the product" \
                                       "has no space or newline surrounding it"

    check_title_no_whitespace_around.severity = 1