from google.appengine.api import mail


class EmailHandler(object):

    def __init__(self, products):
        """
            Ideally, this class would recieve a list of dicts that carry
            the product data. Each dict should have an email address and
            an `errors` key.

            In here we collect all email addresses and use them as keys to
            a dict that would look like
                {
                    email_address: error_messages
                }
            This way we minimize the number of emails sent
        """
        pass

    def send_emails(self):
        """
            We iterate over the list of email/error_messages dicts and send out
            an email for each
        """
        pass

    def format_message(self):
        """
            This formats the email before sending. It would create a template
            that looks something like

                The following products have errors:
                    Product -- Title: Product Title\n, Description: Given Desc
                    Errors -- Title contains whitespace

                    ------------
                    Product -- Title: , Description: Another Desc
                    Errors -- Product does not contain a title
        """
        pass