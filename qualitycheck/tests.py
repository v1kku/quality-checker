import json
import unittest

from validator import DataValidator, ProductDataValidator, InvalidDataException


class DummyDataValidator(DataValidator):

    def check_one(self, object):
        pass

    def check_two(self, object):
        pass

    def checkThree(self, object):
        """
            camelCased methods should also be called
        """

        raise Exception()

    def this_shouldnt_run(self):
        pass


class DataValidatorTestCase(unittest.TestCase):

    def test_non_json_data_is_rejected(self):
        data = "a"

        with self.assertRaises(InvalidDataException):
            DataValidator(data)

            # this is valid json, but it is not valid data
            # in our application
            DataValidator("1")

        with self.assertRaises(NotImplementedError):
            DataValidator({}, data_format="XML")

        DataValidator('{"a": 1}')

    def test_runs_methods_beginning_with_check__(self):
        test_data = {
            "name": "My Name"
        }
        data_validator = DummyDataValidator(json.dumps(test_data))
        data_validator.validate_data()
        self.assertEqual(data_validator.tests_ran, 3)
        self.assertEqual(data_validator.tests_passed, 2)
        self.assertEqual(data_validator.tests_failed, 1)


class ProductValidatorTestCase(unittest.TestCase):

    def assert_invalid_data_fails(self, invalid_data, num_failures_expected=1):
        validator = ProductDataValidator(json.dumps(invalid_data))
        validator.validate_data()
        self.assertEqual(validator.tests_failed, num_failures_expected)

    def assert_valid_data_passes(self, valid_data):
        validator = ProductDataValidator(json.dumps(valid_data))
        validator.validate_data()

        self.assertEqual(validator.tests_failed, 0)
        self.assertEqual(validator.tests_passed, validator.tests_ran)

    def test_required_data_check(self):
        invalid_data = {
            "title": "Title"
        }
        self.assert_invalid_data_fails(invalid_data)

        self.assert_valid_data_passes({
            "title": "Title",
            "description": "This is a product"
        })

    def test_title_whitespace_check(self):
        self.assert_invalid_data_fails({
            "title": r" Test Title\n",
            "description": "Desc"
        })


if __name__ == '__main__':
    unittest.main()
