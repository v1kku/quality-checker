#!/usr/bin/env python
#
# Copyright 2007 Google Inc.
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#
import os
import json

import webapp2
import jinja2

import validator
from messenger import EmailHandler

templates_path = os.path.join(os.path.dirname(os.path.abspath(__file__)), "templates")

jinja_environment = jinja2.Environment(
    loader=jinja2.FileSystemLoader(templates_path))


class MainHandler(webapp2.RequestHandler):
    def get(self):
        template = jinja_environment.get_template("index.html")
        self.response.write(template.render())

    def post(self):
        product_validator = validator.ProductDataValidator(self.request.body)
        product_validator.validate_data()

        if product_validator.tests_failed and product_validator.passed_threshold:
            email_handler = EmailHandler(product_validator.objects_with_errors)
            email_handler.send_emails()

        response_data = {
            "objects_tested": product_validator.objects_tested,
            "checks_ran": product_validator.tests_ran,
            "checks_passed": product_validator.tests_passed,
            "checks_failed": product_validator.tests_failed,
            "was_email_sent": product_validator.passed_threshold,
            "products_with_errors": product_validator.objects_with_errors
        }

        self.response.headers['Content-Type'] = 'application/json'
        self.response.out.write(json.dumps(response_data))



app = webapp2.WSGIApplication([
    ('/', MainHandler),
], debug=True)
