angular.module('qualCheckApp', [])
.controller('MainController', ['$scope', '$http', function($scope, $http){
        $scope.products = [
            {
                title: "",
                description: "",
                email: "email@example.com"
            }
        ];

        $scope.responseData = null;

        $scope.addProduct = function(){
            $scope.products.push({
                title: "",
                description: "",
                email: "email@example.com"
            });
        };

        $scope.submitProducts = function(){
            $http.post('/', $scope.products)
                .success(function(data, status, config){
                    $scope.responseData = data;
                });
        };

        $scope.removeProduct = function(product){
            var p_index = $scope.products.indexOf(product);
            if(p_index > -1){
                $scope.products.splice(p_index, 1);
            }
        };
    }
]);