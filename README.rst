===================================
Product Quality Checker
===================================

The app receives input in form of JSON and runs checks on the input to
see whether the input meets quality standards. It is split up into 3
parts

* The Validator
* The View
* The Messenger

Validation
----------

This contains a class used to verify that data inputed for various objects
meets some quality standards.

* Each check is done in its own function
* Each function that starts with check_* will be run as a check,
  this is similar to how the unittest.Testcase class runs every
  test_* as a test

* The class expects JSON as its input but the logic doesn't care,
  which allows use of other formats in the future, like XML.

* Each check* method has a severity attribute. For every failed check
  this attribute is added to a total. This total is monitored and it is only
  once a set minimum threshold is reached that an email will be sent out with
  the errors. The email is not sent by this class.

* For every object that fails a check, we add to the object the error messages,
  and add the object to a list, this list will be sent back to the user.
* The validator can receive either a single JSON object or a list of them.
* Checks are stateless and only expect data. They therefore can be used offline,
  for example to validate old data that is already in a database.


View
----

For this example, there is a single view which displays a form for entering
product information. More forms can be created by clicking the "Add product"
button.
This view also handles the data submitted by the form, and displays any errors
that were encountered.


Messaging
---------

In this module there is a class that, ideally,would recieve a list of dicts that carry
the product data. Each dict should have an email address and
an `errors` key.

In the messenger's initialization, we collect all email addresses and use them as keys to a dict that would look like below.

``{email_address: error_messages}``

This way we minimize the number of emails sent